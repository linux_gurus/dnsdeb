FROM bitnami/minideb:stretch
MAINTAINER Ricky Neff
RUN apt update
RUN apt upgrade -y
RUN apt install bind9
ENTRYPOINT ["/bin/bash"]
EXPOSE 22
